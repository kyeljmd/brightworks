package org.brightworks.genesis.commons.service.account;

import org.brightworks.genesis.commons.model.account.UserAccount;
import org.springframework.stereotype.Service;

/**
 * Created by kyel on 10/5/2014.
 */
@Service
public interface AccountService {

    UserAccount saveAccountInformation(UserAccount account);
}
