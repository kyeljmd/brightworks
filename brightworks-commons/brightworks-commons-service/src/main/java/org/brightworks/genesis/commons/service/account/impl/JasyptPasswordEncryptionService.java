package org.brightworks.genesis.commons.service.account.impl;

import org.brightworks.genesis.commons.service.account.PasswordEncryptionService;
import org.jasypt.digest.StandardStringDigester;
import org.jasypt.digest.StringDigester;

/**
 * @author kyel
 */
public class JasyptPasswordEncryptionService implements PasswordEncryptionService {

    private static final String HASH_ALGORITHM = "SHA-256";
    private static final int NUM_OF_ITERATIONS = 1000;
    private static final int SALT_SIZE = 16;

    @Override
    public void authenticate(String passwordInput,String storedPassword) throws InvalidCredentialsException {
       boolean doesMatch = passwordDigest().matches(passwordInput, storedPassword);
        if(!doesMatch){
            throw new InvalidCredentialsException("Password Does Not Match");
        }
    }


    @Override
    public String encrypt(String password) {
      return passwordDigest().digest(password);
    }

    private StringDigester passwordDigest(){
        StandardStringDigester stringDigester = new StandardStringDigester();
        stringDigester.setAlgorithm(HASH_ALGORITHM);
        stringDigester.setIterations(NUM_OF_ITERATIONS);
        stringDigester.setSaltSizeBytes(SALT_SIZE);
        stringDigester.initialize();
        return  stringDigester;
    }
}
