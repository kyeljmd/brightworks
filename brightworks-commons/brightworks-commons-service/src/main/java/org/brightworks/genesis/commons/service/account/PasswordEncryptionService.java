package org.brightworks.genesis.commons.service.account;

import org.brightworks.genesis.commons.service.account.impl.InvalidCredentialsException;

/**
 * Created by kyel on 10/5/2014.
 */
public interface PasswordEncryptionService {

    String encrypt(String password);

    void authenticate(String passwordInput,String storedPassword) throws InvalidCredentialsException;
}
