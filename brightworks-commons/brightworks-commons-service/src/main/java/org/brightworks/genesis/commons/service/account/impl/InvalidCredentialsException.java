package org.brightworks.genesis.commons.service.account.impl;

/**
 * @author  by kyel on 10/5/2014.
 */
public class InvalidCredentialsException extends  Exception {

    public InvalidCredentialsException(String message){
        super(message);
    }
}
