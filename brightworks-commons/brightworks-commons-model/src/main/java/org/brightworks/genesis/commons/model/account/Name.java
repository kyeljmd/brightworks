package org.brightworks.genesis.commons.model.account;

import org.brightworks.genesis.commons.model.JpaModel;

import javax.persistence.*;

/**
 * @author  by kyeljmd
 */
@Table(name = "PERSON_NAME")
@Entity
public class Name extends JpaModel {

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "MIDDLE_NAME")
    private String middleName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @OneToOne(fetch= FetchType.LAZY)
    private UserAccount account;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
