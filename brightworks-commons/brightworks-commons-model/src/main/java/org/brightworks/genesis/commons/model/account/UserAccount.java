package org.brightworks.genesis.commons.model.account;

import org.brightworks.genesis.commons.model.JpaModel;

import javax.persistence.*;

/**
 * @author by kyeljmd
 */
@Table(name="USER_ACCOUNT")
@Entity
public class UserAccount extends JpaModel {

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "PASSWORD")
    private String password;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NAME_ID")
    private Name name;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }
}
