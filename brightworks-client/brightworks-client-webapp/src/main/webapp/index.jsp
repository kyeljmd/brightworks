<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Brightworks Kurt">
    <title>Kenlex Telecoms | Login</title>
    <!-- Bootstrap Core CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/login-page.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="${pageContext.request.contextPath}/resources/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
 </head>
<body>
<!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Kenlex Telecoms</a>
        </div>
    </div>
</nav>
<!-- Background Image -->
<img class="bg" src="${pageContext.request.contextPath}/resources/img/login-bg.jpg">
<!-- Page Content -->
<div class="container">
    <div class="content pull-right">
        <form class="form-signin"action="">
            <fieldset>
                <input class="form-control form-group" type="text" placeholder="Username">
                <input class="form-control" type="password" placeholder="Password" >
                <a class="forgot pull-right" href="#">Forgot password?</a>
                <button class="btn btn-block btn-primary" type="submit">Sign in</button>
            </fieldset>
        </form>
    </div>
</div>
<!-- /.container -->
</body>
</html>