package com.brightworks.genesis.client.webapp;

import org.brightworks.genesis.commons.service.account.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by kyel on 10/12/2014.
 */
@Controller
public class HelloWorldController  {
    @Autowired
    private AccountService accountService;

    @RequestMapping("/helloWorld")
    public String helloWorld(Model model) {
        model.addAttribute("message", "Hello World!");
        return "helloWorld";
    }
}
